"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_organisms_store views *

:details: lara_django_organisms_store views module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from dataclasses import dataclass, field
from typing import List

from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView

from django_tables2 import SingleTableView

from .forms import OrganismInstanceCreateForm, OrganismInstanceUpdateForm
from .tables import OrganismInstanceTable
from .models import OrganismInstance

# Create your  lara_django_organisms_store views here.

@dataclass
class OrganismsStoreMenu:
    menu_items:  List[dict] = field(default_factory=lambda: [
        {'name': 'Organisms',
         'path': 'lara_django_organisms:organism-list'},
        {'name': 'Organisms-Store',
         'path': 'lara_django_organisms_store:organism-list'},
    ])


class OrganismSingleTableView(SingleTableView):
    model = OrganismInstance
    table_class = OrganismInstanceTable

    #fields = ('name', 'name_full', 'URL', 'handle', 'IRI', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'specifications', 'spec_JSON', 'spec_doc', 'brochure', 'quickstart', 'manual', 'service_manual', 'icon', 'image', 'description', 'organism_id', 'organism_class', 'shape')

    template_name = 'lara_django_organisms_store/list.html'
    success_url = '/organisms-store/organism/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Organism Store - List"
        context['create_link'] = 'lara_django_organisms_store:organism-create'
        context['menu_items'] = OrganismsStoreMenu().menu_items
        return context


class OrganismDetailView(DetailView):
    model = OrganismInstance

    template_name = 'lara_django_organisms_store/organism_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Organism Store - Details"
        context['update_link'] = 'lara_django_organisms_store:organism-update'
        context['menu_items'] = OrganismsStoreMenu().menu_items
        return context


class OrganismCreateView(CreateView):
    model = OrganismInstance

    template_name = 'lara_django_organisms_store/create_form.html'
    form_class = OrganismInstanceCreateForm
    success_url = '/organisms-store/organism/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Organism Store - Create"
        return context


class OrganismUpdateView(UpdateView):
    model = OrganismInstance

    template_name = 'lara_django_organisms_store/update_form.html'
    form_class = OrganismInstanceUpdateForm
    success_url = '/organisms-store/organism/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Organism Store - Update"
        context['delete_link'] = 'lara_django_organisms_store:organism-delete'
        return context


class OrganismDeleteView(DeleteView):
    model = OrganismInstance

    template_name = 'lara_django_organisms_store/delete_form.html'
    success_url = '/organisms-store/organism/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Organism Store - Delete"
        context['delete_link'] = 'lara_django_organisms_store:organism-delete'
        return context