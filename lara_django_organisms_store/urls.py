"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_organisms_store urls *

:details: lara_django_organisms_store urls module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from django.views.generic import TemplateView
from django_socio_grpc.settings import grpc_settings
#import lara_django.urls as base_urls

from . import views

# Add your {cookiecutter.project_slug}} urls here.


# !! this sets the apps namespace to be used in the template
app_name = "lara_django_organisms_store"

# companies and institutions should also be added
# the 'name' attribute is used in templates to address the url independent of the view

urlpatterns = [
    path('organism/list/', views.OrganismSingleTableView.as_view(), name='organism-list'),
    path('organism/create/', views.OrganismCreateView.as_view(), name='organism-create'),
    path('organism/update/<uuid:pk>', views.OrganismUpdateView.as_view(), name='organism-update'),
    path('organism/delete/<uuid:pk>', views.OrganismDeleteView.as_view(), name='organism-delete'),
    path('organism/<uuid:pk>/', views.OrganismDetailView.as_view(), name='organism-detail'),
    path('', views.OrganismSingleTableView.as_view(), name='organisms-root'),
    # path('subdir/', include('.urls')),
] # ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# register handlers in settings
grpc_settings.user_settings["GRPC_HANDLERS"] = [
    "lara_django_organisms_store.grpc_if.handlers.grpc_handlers"]
