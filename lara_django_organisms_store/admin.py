"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_organisms_store admin *

:details: lara_django_organisms_store admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev admin_generator lara_django_organisms_store >> admin.py" to update this file
________________________________________________________________________
"""
# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import ExtraData, OrganismInstance, OrganismOrderItem, OrganismWishlist, OrganismBasket, OrganismOrder, OrganismLocalStore


@admin.register(ExtraData)
class ExtraDataAdmin(admin.ModelAdmin):
    list_display = (
        'data_type',
        'namespace',
        'URI',
        'text',
        'XML',
        'JSON',
        'bin',
        'media_type',
        'IRI',
        'URL',
        'description',
        'extra_data_id',
        'file',
    )
    list_filter = ('data_type', 'namespace', 'media_type')


@admin.register(OrganismInstance)
class OrganismInstanceAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name',
        'name_full',
        'barcode1D',
        'barcode2D',
        'UUID',
        'URL',
        'handle',
        'IRI',
        'registration_no',
        'vendor',
        'product_id',
        'serial_no',
        'service_no',
        'manufacturing_date',
        'price',
        'currency',
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
        'hash_SHA256',
        'description',
        'organism_inst_id',
        'organism',
        'image',
        'literature',
    )
    list_filter = (
        'namespace',
        'vendor',
        'manufacturing_date',
        'currency',
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
        'organism',
    )
    raw_id_fields = ('tags', 'extra_data')
    search_fields = ('name',)


@admin.register(OrganismOrderItem)
class OrganismOrderItemAdmin(admin.ModelAdmin):
    list_display = (
        'quantity',
        'item_price_net',
        'item_price_gross',
        'item_shipping_price',
        'item_price_tax',
        'item_toll',
        'toll_number',
        'item_discount',
        'total_price_net',
        'organism_order_item_id',
        'organism_instance',
    )
    list_filter = ('organism_instance',)
    raw_id_fields = ('extra_data',)


@admin.register(OrganismWishlist)
class OrganismWishlistAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'organism_wishlist_id',
    )
    list_filter = ('namespace', 'user', 'date_created', 'date_modified')
    raw_id_fields = ('organism_wish',)


@admin.register(OrganismBasket)
class OrganismBasketAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'device_basket_id',
    )
    list_filter = ('namespace', 'user', 'date_created', 'date_modified')
    raw_id_fields = ('organisms',)


@admin.register(OrganismOrder)
class OrganismOrderAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'IRI',
        'user_ordered',
        'user_ordering',
        'order_date',
        'order_id_internal',
        'order_id_external',
        'order_barcode',
        'order_company',
        'order_quote_id',
        'order_quote_date',
        'order_invoice_id',
        'order_invoice_date',
        'shipping_company',
        'shipping_tracking_id',
        'shipping_price',
        'budget_URL',
        'order_pay_date',
        'order_total_price_net',
        'order_total_price_gross',
        'order_total_price_tax',
        'order_total_price_toll',
        'order_total_price_currency',
        'order_state',
        'organism_order_id',
    )
    list_filter = (
        'namespace',
        'user_ordered',
        'user_ordering',
        'order_date',
        'order_company',
        'order_quote_date',
        'order_invoice_date',
        'shipping_company',
        'order_pay_date',
        'order_total_price_currency',
        'order_state',
    )
    raw_id_fields = ('organism_order_items', 'extra_data')


@admin.register(OrganismLocalStore)
class OrganismLocalStoreAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'organism_localstore_id',
        'location',
    )
    list_filter = (
        'namespace',
        'user',
        'date_created',
        'date_modified',
        'location',
    )
    raw_id_fields = ('organism_instances',)
