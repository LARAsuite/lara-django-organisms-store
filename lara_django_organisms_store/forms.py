"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_organisms_store admin *

:details: lara_django_organisms_store admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev forms_generator -c lara_django_organisms_store > forms.py" to update this file
________________________________________________________________________
"""
# django crispy forms s. https://github.com/django-crispy-forms/django-crispy-forms for []
# generated with django-extensions forms_generator -c  [] > forms.py (LARA-version)

from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column

from .models import ExtraData, OrganismInstance, OrganismOrderItem, OrganismWishlist, OrganismBasket, OrganismOrder, OrganismLocalStore


class ExtraDataCreateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file',
            Submit('submit', 'Create')
        )


class ExtraDataUpdateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file',
            Submit('submit', 'Update')
        )


class OrganismInstanceCreateForm(forms.ModelForm):
    class Meta:
        model = OrganismInstance
        fields = (
            'namespace',
            'name',
            'name_full',
            'barcode1D',
            'barcode2D',
            'UUID',
            'URL',
            'handle',
            'IRI',
            'registration_no',
            'vendor',
            'serial_no',
            'service_no',
            'manufacturing_date',
            'price',
            'currency',
            'purchase_date',
            'date_delivery',
            'end_of_warranty_date',
            'location',
            'hash_SHA256',
            'description',
            'organism',
            'image',
            'literature')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name',
            'name_full',
            'barcode1D',
            'barcode2D',
            'UUID',
            'URL',
            'handle',
            'IRI',
            'registration_no',
            'vendor',
            'serial_no',
            'service_no',
            'manufacturing_date',
            'price',
            'currency',
            'purchase_date',
            'date_delivery',
            'end_of_warranty_date',
            'location',
            'hash_SHA256',
            'description',
            'organism',
            'image',
            'literature',
            Submit('submit', 'Create')
        )


class OrganismInstanceUpdateForm(forms.ModelForm):
    class Meta:
        model = OrganismInstance
        fields = (
            'namespace',
            'name',
            'name_full',
            'barcode1D',
            'barcode2D',
            'UUID',
            'URL',
            'handle',
            'IRI',
            'registration_no',
            'vendor',
            'serial_no',
            'service_no',
            'manufacturing_date',
            'price',
            'currency',
            'purchase_date',
            'date_delivery',
            'end_of_warranty_date',
            'location',
            'hash_SHA256',
            'description',
            'organism',
            'image',
            'literature')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name',
            'name_full',
            'barcode1D',
            'barcode2D',
            'UUID',
            'URL',
            'handle',
            'IRI',
            'registration_no',
            'vendor',
            'serial_no',
            'service_no',
            'manufacturing_date',
            'price',
            'currency',
            'purchase_date',
            'date_delivery',
            'end_of_warranty_date',
            'location',
            'hash_SHA256',
            'description',
            'organism',
            'image',
            'literature',
            Submit('submit', 'Update')
        )


class OrganismOrderItemCreateForm(forms.ModelForm):
    class Meta:
        model = OrganismOrderItem
        fields = (
            'quantity',
            'item_price_net',
            'item_price_gross',
            'item_shipping_price',
            'item_price_tax',
            'item_toll',
            'toll_number',
            'item_discount',
            'total_price_net',
            'organism_instance')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'quantity',
            'item_price_net',
            'item_price_gross',
            'item_shipping_price',
            'item_price_tax',
            'item_toll',
            'toll_number',
            'item_discount',
            'total_price_net',
            'organism_instance',
            Submit('submit', 'Create')
        )


class OrganismOrderItemUpdateForm(forms.ModelForm):
    class Meta:
        model = OrganismOrderItem
        fields = (
            'quantity',
            'item_price_net',
            'item_price_gross',
            'item_shipping_price',
            'item_price_tax',
            'item_toll',
            'toll_number',
            'item_discount',
            'total_price_net',
            'organism_instance')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'quantity',
            'item_price_net',
            'item_price_gross',
            'item_shipping_price',
            'item_price_tax',
            'item_toll',
            'toll_number',
            'item_discount',
            'total_price_net',
            'organism_instance',
            Submit('submit', 'Update')
        )


class OrganismWishlistCreateForm(forms.ModelForm):
    class Meta:
        model = OrganismWishlist
        fields = (
            'namespace',
            'name_full',
            'user',
            'date_created',
            'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name_full',
            'user',
            'date_created',
            'date_modified',
            Submit('submit', 'Create')
        )


class OrganismWishlistUpdateForm(forms.ModelForm):
    class Meta:
        model = OrganismWishlist
        fields = (
            'namespace',
            'name_full',
            'user',
            'date_created',
            'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name_full',
            'user',
            'date_created',
            'date_modified',
            Submit('submit', 'Update')
        )


class OrganismBasketCreateForm(forms.ModelForm):
    class Meta:
        model = OrganismBasket
        fields = (
            'namespace',
            'name_full',
            'user',
            'date_created',
            'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name_full',
            'user',
            'date_created',
            'date_modified',
            Submit('submit', 'Create')
        )


class OrganismBasketUpdateForm(forms.ModelForm):
    class Meta:
        model = OrganismBasket
        fields = (
            'namespace',
            'name_full',
            'user',
            'date_created',
            'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name_full',
            'user',
            'date_created',
            'date_modified',
            Submit('submit', 'Update')
        )


class OrganismOrderCreateForm(forms.ModelForm):
    class Meta:
        model = OrganismOrder
        fields = (
            'namespace',
            'name_full',
            'IRI',
            'user_ordered',
            'user_ordering',
            'order_barcode',
            'order_company',
            'order_quote_date',
            'order_invoice_date',
            'shipping_company',
            'shipping_price',
            'budget_URL',
            'order_total_price_net',
            'order_total_price_gross',
            'order_total_price_tax',
            'order_total_price_toll',
            'order_total_price_currency',
            'order_state')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name_full',
            'IRI',
            'user_ordered',
            'user_ordering',
            'order_barcode',
            'order_company',
            'order_quote_date',
            'order_invoice_date',
            'shipping_company',
            'shipping_price',
            'budget_URL',
            'order_total_price_net',
            'order_total_price_gross',
            'order_total_price_tax',
            'order_total_price_toll',
            'order_total_price_currency',
            'order_state',
            Submit('submit', 'Create')
        )


class OrganismOrderUpdateForm(forms.ModelForm):
    class Meta:
        model = OrganismOrder
        fields = (
            'namespace',
            'name_full',
            'IRI',
            'user_ordered',
            'user_ordering',
            'order_barcode',
            'order_company',
            'order_quote_date',
            'order_invoice_date',
            'shipping_company',
            'shipping_price',
            'budget_URL',
            'order_total_price_net',
            'order_total_price_gross',
            'order_total_price_tax',
            'order_total_price_toll',
            'order_total_price_currency',
            'order_state')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name_full',
            'IRI',
            'user_ordered',
            'user_ordering',
            'order_barcode',
            'order_company',
            'order_quote_date',
            'order_invoice_date',
            'shipping_company',
            'shipping_price',
            'budget_URL',
            'order_total_price_net',
            'order_total_price_gross',
            'order_total_price_tax',
            'order_total_price_toll',
            'order_total_price_currency',
            'order_state',
            Submit('submit', 'Update')
        )


class OrganismLocalStoreCreateForm(forms.ModelForm):
    class Meta:
        model = OrganismLocalStore
        fields = (
            'namespace',
            'name_full',
            'user',
            'date_created',
            'date_modified',
            'location')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name_full',
            'user',
            'date_created',
            'date_modified',
            'location',
            Submit('submit', 'Create')
        )


class OrganismLocalStoreUpdateForm(forms.ModelForm):
    class Meta:
        model = OrganismLocalStore
        fields = (
            'namespace',
            'name_full',
            'user',
            'date_created',
            'date_modified',
            'location')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name_full',
            'user',
            'date_created',
            'date_modified',
            'location',
            Submit('submit', 'Update')
        )

# from .forms import ExtraDataCreateForm, OrganismInstanceCreateForm, OrganismOrderItemCreateForm, OrganismWishlistCreateForm, OrganismBasketCreateForm, OrganismOrderCreateForm, OrganismLocalStoreCreateFormExtraDataUpdateForm, OrganismInstanceUpdateForm, OrganismOrderItemUpdateForm, OrganismWishlistUpdateForm, OrganismBasketUpdateForm, OrganismOrderUpdateForm, OrganismLocalStoreUpdateForm
