"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_organisms_store admin *

:details: lara_django_organisms_store admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev tables_generator lara_django_organisms_store >> tables.py" to update this file
________________________________________________________________________
"""
# django Tests s. https://docs.djangoproject.com/en/4.1/topics/testing/overview/ for lara_django_organisms_store
# generated with django-extensions tests_generator  lara_django_organisms_store > tests.py (LARA-version)

import logging
import django_tables2 as tables


from .models import ExtraData, OrganismInstance, OrganismOrderItem, OrganismWishlist, OrganismBasket, OrganismOrder, OrganismLocalStore

class ExtraDataTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_organisms_store:extradata-detail', [tables.A('pk')]))

    class Meta:
        model = ExtraData

        fields = (
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'bin',
                'media_type',
                'IRI',
                'URL',
                'description',
                'file')

class OrganismInstanceTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name  = tables.Column(linkify=('lara_django_organisms_store:organism-detail', [tables.A('pk')]))

    class Meta:
        model = OrganismInstance

        fields = (
                'namespace',
                'name',
                'name_full',
                'barcode1D',
               
               
                'URL',
             
            
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
             
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                
                'description',
                'organism',
                'image',
                'literature')

class OrganismOrderItemTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_organisms_store:organismorderitem-detail', [tables.A('pk')]))

    class Meta:
        model = OrganismOrderItem

        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'organism_instance')

class OrganismWishlistTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_organisms_store:organismwishlist-detail', [tables.A('pk')]))

    class Meta:
        model = OrganismWishlist

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

class OrganismBasketTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_organisms_store:organismbasket-detail', [tables.A('pk')]))

    class Meta:
        model = OrganismBasket

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

class OrganismOrderTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_organisms_store:organismorder-detail', [tables.A('pk')]))

    class Meta:
        model = OrganismOrder

        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_date',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_pay_date',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

class OrganismLocalStoreTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_organisms_store:organismlocalstore-detail', [tables.A('pk')]))

    class Meta:
        model = OrganismLocalStore

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

