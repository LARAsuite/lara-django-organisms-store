"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_organisms_store models *

:details: lara_django_organisms_store database models.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - remove unwanted models/fields
________________________________________________________________________
"""

from django.db import models

import uuid

from django.conf import settings

from django.db import models
from lara_django_base.models import DataType, MediaType, ExtraDataAbstr, Namespace, Tag, Location
from lara_django_store.models import ItemInstanceAbstr, OrderItemAbstr, ItemOrderAbstr, ItemBasketAbstr

from lara_django_organisms.models import Organism


settings.FIXTURES += []


class ExtraData(ExtraDataAbstr):
    """This class can be used to extend data, by extra information,
       e.g. more telephone numbers, customer numbers, ... """

    extra_data_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    file = models.FileField(
        upload_to='organisms_store', blank=True, null=True, help_text="rel. path/filename")


class OrganismInstance(ItemInstanceAbstr):
    """ Organism Instance Model, e.g. a specific plant, animal, fungus, bacteria / strain,  ...
    :param ItemInstanceAbstr: _description_
    :type ItemInstanceAbstr: _type_
    """
    organism_inst_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)

    organism = models.ForeignKey(Organism, related_name='%(app_label)s_%(class)s_materials_related',
                                 related_query_name="%(app_label)s_%(class)s_materials_related_query",
                                 on_delete=models.CASCADE)

    image = models.ImageField(upload_to='material_store/', blank=True, default="",
                              help_text="rel. path/filename to image")

    extra_data = models.ManyToManyField(ExtraData, blank=True, related_name='%(app_label)s_%(class)s_oi_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_oi_extra_data_related_query",
                                        help_text="extra data")
    literature = models.URLField(
        blank=True, null=True, help_text="literature regarding this part instance")
    # literature = models.ManyToManyField(LibItem, related_name="%(app_label)s_%(class)s_literature_related",
    #                                     related_query_name="%(app_label)s_%(class)s_literature_related_query",  blank=True,
    #                                     help_text="literature regarding this part instance")

    def __str__(self):
        return self.organism.name or ""

    def __repr__(self):
        return self.organism.name or ""


    def save(self,  *args, force_insert=None, using=None, **kwargs):
        """ Here we generate some default values for full_name """
        if self.name is None or self.name == "" :
            self.name = self.organism.name

        
        if self.name_full is None or self.name_full == "":
            if self.namespace is not None:
               self.name_full = f"{self.namespace.name}/" + "_".join((self.name.replace(' ', '_') , self.registration_no, self.barcode1D))
            else:
                self.name_full = "_".join((self.name, self.registration_no, self.barcode1D))
            
        super().save(*args, force_insert=force_insert, using=using,  **kwargs)


class OrganismOrderItem(OrderItemAbstr):
    """ Organism Order Item Model for storing device-order-items, """
    organism_order_item_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    organism_instance = models.ForeignKey(OrganismInstance, related_name='%(app_label)s_%(class)s_organism_instances_related',
                                          related_query_name="%(app_label)s_%(class)s_organism_instances_related_query",
                                          on_delete=models.CASCADE)
    extra_data = models.ManyToManyField(ExtraData, blank=True, related_name='%(app_label)s_%(class)s_orgi_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_orgi_extra_data_related_query",
                                        help_text="additional data for this order item")

    def __str__(self):
        return self.device.name_full or ""

    def __repr__(self):
        return self.device.name_full or ""

    class Meta:
        # ordering = ['-date_created']
        # verbose_name = "Organism Order Item"
        verbose_name_plural = "Organism Order Items"


class OrganismWishlist(ItemBasketAbstr):
    """ Organism Wishlist Model for storing device-wishlists, """
    organism_wishlist_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    organism_wish = models.ManyToManyField(OrganismOrderItem, related_name='%(app_label)s_%(class)s_organism_wish_related',
                                           related_query_name="%(app_label)s_%(class)s_organism_wish_related_query",
                                           blank=True, help_text="organisms in wishlist")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    # class Meta:
        # ordering = ['-date_created']
        # verbose_name = "Organism Wishlist"
    #    verbose_name_plural = "Organism Wishlists"


class OrganismBasket(ItemBasketAbstr):
    """ Organism Basket Model for storing organism-baskets, """
    device_basket_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    organisms = models.ManyToManyField(OrganismOrderItem, related_name='%(app_label)s_%(class)s_organism_basket_related',
                                       related_query_name="%(app_label)s_%(class)s_organism_basket_related_query",
                                       blank=True, help_text="organism in basket")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    # class Meta:
        # ordering = ['-date_created']
        # verbose_name = "Organism Basket"
    #    verbose_name_plural = "Organism Baskets"


class OrganismOrder(ItemOrderAbstr):
    """ Organism Order Model for storing organism-orders, """
    organism_order_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    organism_order_items = models.ManyToManyField(OrganismOrderItem, related_name='%(app_label)s_%(class)s_organism_order_related',
                                                  related_query_name="%(app_label)s_%(class)s_organism_order_related_query",
                                                  blank=True, help_text="organisms in order")
    extra_data = models.ManyToManyField(ExtraData, blank=True, related_name='%(app_label)s_%(class)s_orgo_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_orgo_extra_data_related_query",
                                        help_text="additional data for this order")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    # class Meta:
        # ordering = ['-date_created']


class OrganismLocalStore(ItemBasketAbstr):
    """ Organism LocalStore Model for storing organism-local-stores,
        e.g. a local store of a lab, a company, a university, etc.
    """
    organism_localstore_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    organism_instances = models.ManyToManyField(OrganismInstance, related_name='%(app_label)s_%(class)s_org_instance_loc_related',
                                                related_query_name="%(app_label)s_%(class)s_org_instance_loc_related_query",
                                                blank=True, help_text="parts in local store")
    location = models.ForeignKey(Location, related_name='%(app_label)s_%(class)s_org_location_loc_related',
                                 related_query_name="%(app_label)s_%(class)s_org_location_loc_related_query",
                                 on_delete=models.CASCADE)

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""
